from conans import ConanFile, CMake, tools
import shutil

class HiredisConan(ConanFile):
    name            = "hiredis"
    version         = "0.14.1"
    license         = "BSD-3-Clause"
    author          = "toge.mail@gmail.com"
    url             = ""
    homepage        = "https://github.com/redis/hiredis"
    description     = "Hiredis is a minimalistic C client library for the Redis database."
    topics          = ("database", "minimalistic", "hiredis")
    settings        = "os", "compiler", "build_type", "arch"
    options         = {"shared": [True, False]}
    default_options = {"shared": False}
    generators      = "cmake"
    exports         = ["CMakeLists.txt"]

    def source(self):
        tools.get("https://github.com/redis/hiredis/archive/v{}.zip".format(self.version))
        shutil.move("hiredis-{}".format(self.version), "hiredis")
        shutil.move("CMakeLists.txt", "hiredis/CMakeLists.txt")

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder="hiredis")
        cmake.build()

        # Explicit way:
        # self.run('cmake %s/hello %s'
        #          % (self.source_folder, cmake.command_line))
        # self.run("cmake --build . %s" % cmake.build_config)

    def package(self):
        self.copy("*.h", dst="include", src="hiredis", excludes="example*.h")
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["hiredis"]

