list(APPEND SRCS net.c hiredis.c sds.c async.c read.c alloc.c dict.c)
include_directories(".")

add_library(hiredis STATIC ${SRCS})
